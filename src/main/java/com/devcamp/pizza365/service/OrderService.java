package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.Order;
import com.devcamp.pizza365.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository orderRepository;
    public List<Order> getAllOrders(){
        List<Order> orders = new ArrayList<Order>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }

}
