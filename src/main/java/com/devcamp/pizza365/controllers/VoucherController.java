package com.devcamp.pizza365.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.Voucher;
import com.devcamp.pizza365.service.VoucherService;

@RestController
@CrossOrigin
public class VoucherController {
    @Autowired
    VoucherService voucherService;
    @GetMapping("/vouchers")
    public ResponseEntity<List<Voucher>> getVouchers(){
        try {
            return new ResponseEntity<>(voucherService.getVouchers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
